<!-- ANIMATED COLUMNS 
================================================== -->
<div class="row">
	<div class="twelve columns">
		<ul class="ca-menu">
			<li>
			<a href="#">
			<span class="ca-icon">F</span>
			<div class="ca-content">
				<h2 class="ca-main">Responsive<br/> Design</h2>
				<h3 class="ca-sub">Across all major devices</h3>
			</div>
			</a>
			</li>
			<li>
			<a href="#">
			<span class="ca-icon">H</span>
			<div class="ca-content">
				<h2 class="ca-main">Friendly<br/> Documentation</h2>
				<h3 class="ca-sub">Straight to the point</h3>
			</div>
			</a>
			</li>
			<li>
			<a href="#">
			<span class="ca-icon">N</span>
			<div class="ca-content">
				<h2 class="ca-main">Alternate<br/> Home Pages</h2>
				<h3 class="ca-sub">Full slider, boxed or none</h3>
			</div>
			</a>
			</li>
			<li>
			<a href="#">
			<span class="ca-icon">K</span>
			<div class="ca-content">
				<h2 class="ca-main">Filterable<br/> Portofolio</h2>
				<h3 class="ca-sub">Isotop & PrettyPhoto</h3>
			</div>
			</a>
			</li>
		</ul>
	</div>
</div>