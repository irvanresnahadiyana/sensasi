<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	
	<script type='text/javascript'>
	$(document).ready(function () {
		if ($("[rel=tooltip]").length) {
		$("[rel=tooltip]").tooltip();
		}
	});
	</script>
	<?php
	  $baseUrl = Yii::app()->theme->baseUrl; 
	  $cs = Yii::app()->getClientScript();
	  Yii::app()->clientScript->registerCoreScript('jquery');
	?>
	
    <!-- the styles -->
    <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl;?>/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl;?>/css/bootstrap-responsive.min.css">
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Pontano+Sans'>
    
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
	
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles_admin.css" />
	
</head>

<body>
<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container-fluid">
		<div style="color:#fff; margin-top:12px; font-size: 12px" class="pull-right">
		<div style="float: left; margin-right: 10px; margin-top: -10px">
			<a href="<?php echo Yii::app()->controller->createUrl('site/index');?>" target="_blank" class="btn btn-success btn-small"><i class="icon-eye-open icon-white"></i> Halaman Depan</a>
		</div>
			Hallo, <span style="color:#DD4B39; font-weight:bold"><?php //echo User::model()->findByPk(Yii::app()->user->id)->username;?></span> | <a href="<?php echo Yii::app()->controller->createUrl('/site/logout');?>"><b>Logout</b></a>	
		</div>
		<a href="#" class="brand"><span class="first"><?php echo Yii::app()->name;?></span></a>
		</div>
	</div>
</div>
<div id="page" class="container-fluid">
	<div class="row-fluid">
		<div id="left"  class="span2">
			<legend><h2><strong>Main Menu</strong></h2></legend>
			
			<?php $this->widget('bootstrap.widgets.TbMenu', array(
			'type'=>'list',
			'stacked'=>true,
			'items'=>array(
				
				array('label'=>'Dashboard','icon'=>'th white','url'=>array('site/administrator')),
				array('label'=>'Content Home','icon'=>'list white','url'=>array('home/update','id'=>2)),
				array('label'=>'About Us','icon'=>'hand-right white','url'=>array('aboutUs/admin')),				
				array('label'=>'Event','icon'=>'book white','url'=>array('event/admin')),
				array('label'=>'Gallery','icon'=>'picture white','url'=>array('gallery/admin')),
				array('label'=>'Video','icon'=>'play white','url'=>array('video/admin')),
				array('label'=>'Contact Us','icon'=>'envelope white','url'=>array('contacUs/admin')),
				array('label'=>'Slider','icon'=>'picture white','url'=>array('slider/admin')),
				array('label'=>'Sosmed','icon'=>'cog white','url'=>array('sosmed/admin')),
				array('label'=>'User','icon'=>'user white','url'=>array('user/admin')),
				array('label'=>'User Role','icon'=>'ok white','url'=>array('userRole/admin')),
				array('label'=>'Logout','icon'=>'cog white','url'=>array('site/logout')),
				
				
			),
		)); ?>

		</div>
		
		<!-- Javascript ChartJS-->
		<script language="JavaScript" type="text/JavaScript">
			$(document).ready(function(){
				$("#accordian h3").click(function(){
					//slide up all the link lists
					$("#accordian ul ul").slideUp();
					//slide down the link list below the h3 clicked - only if its closed
					if(!$(this).next().is(":visible"))
					{
						$(this).next().slideDown();
					}
				})
			})
		</script>
		<div id="right" class="span10">
			<?php echo $content; ?>
		</div>
	</div>
	<!--Footer Panel -->
	<div class="clear"></div>
	<div id="footer">	
		Copyright &copy <?php echo date('Y');?> Digital Data Studio
	</div><!-- footer -->
</div><!--page-->

</body>
</html>
