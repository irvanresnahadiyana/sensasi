<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->

<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->

<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->

<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->

<!--[if gt IE 8]><!-->

<html class="no-js" lang="en">

<!--<![endif]-->

<head>

<meta charset="utf-8"/>

<!-- Set the viewport width to device width for mobile -->

<meta name="viewport" content="width=device-width"/>

<?php

	  $baseUrl = Yii::app()->theme->baseUrl; 

	  $cs = Yii::app()->getClientScript();

	  Yii::app()->clientScript->registerCoreScript('jquery');

	?>

<title>Sensasi Terabax's</title>

<!-- CSS Files-->

<link rel="stylesheet" href="<?php echo $baseUrl;?>/css/style.css">

<link rel="stylesheet" href="<?php echo $baseUrl;?>/css/homepage.css"><!-- homepage stylesheet -->

<link rel="stylesheet" href="<?php echo $baseUrl;?>/css/skins/red.css"><!-- skin color -->

<link rel="stylesheet" href="<?php echo $baseUrl;?>/css/responsive.css">

<link rel="stylesheet" href="<?php echo $baseUrl;?>/css/prettyPhoto.css">



<!-- IE Fix for HTML5 Tags -->

<!--[if lt IE 9]>

    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>

  <![endif]-->  

<link href="<?php echo Yii::app()->request->baseUrl; ?>/images/ico.png" rel="icon" type="image/x-icon"/>
</head>

<body>

<!-- HEADER