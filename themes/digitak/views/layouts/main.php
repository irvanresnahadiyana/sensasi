<!-- Require the header -->
<?php require_once('tpl_header.php')?>

<!-- Require the navigation -->
<?php require_once('tpl_menu.php')?>

<!-- Require the animasi -->
<?php //require_once('tpl_animasi.php')?>

<!-- Include content pages -->
<?php echo $content; ?>

<!-- Require the footer -->
<?php require_once('tpl_footer.php')?>
