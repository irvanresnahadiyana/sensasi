<?php

	  $baseUrl = Yii::app()->theme->baseUrl; 

	  $cs = Yii::app()->getClientScript();

	  Yii::app()->clientScript->registerCoreScript('jquery');

	?>

<!-- SLIDER 

================================================== -->

<div id="ei-slider" class="ei-slider posisi">

	<ul class="ei-slider-large">

	<?php foreach(Slider::model()->findAll() as $model) { ?>

		<li>

		<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/slider/<?php echo $model->file; ?>" alt="image01" class="responsiveslide">

		</li>

	<?php } ?>

	</ul>

		<!-- slider-thumbs -->

	<ul class="ei-slider-thumbs">

		<li class="ei-slider-element">Current</li>

		<?php foreach(Slider::model()->findAll() as $model) { ?>

			<li><a href="#">Slide 1</a><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/slider/<?php echo $model->file; ?>" class="slideshowthumb" alt="thumb01"/></li>

		<?php } ?>

	</ul>

</div>

<!--<div class="minipause">

</div>-->

<!-- SUBHEADER

================================================== -->

<div id="subheader">

	<div class="row">

		<div class="twelve columns">

		</div>

	</div>

</div>



<!-- CONTENT 

================================================== -->

<div class="row">

	<div class="twelve columns">

		<div class="centersectiontitle">
			<h4>Event</h4>
			<hr class="line">
		</div>

	</div>

	<br>

	<br>

	<div class="twelve columns">
			<?php foreach(Event::model()->findAll(array('order'=>'id ASC, status=0'  )) as $model) { ?>
			<div class="four columns events ">
					<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/event/<?php echo $model->foto; ?>">
					<p><?php echo substr($model->deskripsi,0,70).'...';?><?php echo CHtml::link('More', array('/site/detailEvent/', 'id'=>$model->id));?></p>
			</div>
			<?php } ?>
			
		</div>

<div class="hr">

</div>



