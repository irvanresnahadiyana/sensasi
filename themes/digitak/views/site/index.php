<?php
	  $baseUrl = Yii::app()->theme->baseUrl; 
	  $cs = Yii::app()->getClientScript();
	  Yii::app()->clientScript->registerCoreScript('jquery');
	?>
<!-- SLIDER 
================================================== -->
<div id="ei-slider" class="ei-slider posisi">
	<ul class="ei-slider-large">
	<?php foreach(Slider::model()->findAll() as $model) { ?>
		<li>
		<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/slider/<?php echo $model->file; ?>" alt="image01" class="responsiveslide">
		</li>
	<?php } ?>
	</ul>
	<!-- slider-thumbs -->
		<!-- slider-thumbs -->
	<ul class="ei-slider-thumbs">
		<li class="ei-slider-element">Current</li>
		<?php foreach(Slider::model()->findAll() as $model) { ?>
			<li><a href="#">Slide 1</a><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/slider/<?php echo $model->file; ?>" class="slideshowthumb" alt="thumb01"/></li>
		<?php } ?>
	</ul>
</div>
<!--<div class="minipause">
</div>-->
<!-- SUBHEADER
================================================== -->
<div id="subheader">
	<div class="row">
		<div class="twelve columns">
		</div>
	</div>
</div>

<!-- CONTENT 
================================================== -->
<div class="row">
	<!--<div class="twelve columns">
		<div class="centersectiontitle">
			<h4>What we do</h4>
		</div>
	</div>-->
	<br>
	<br>
	<div class="seven columns new-event">
		<h4>New Event</h4>
		<hr class="line">
	</div>
	<br>
	<br>
	<div id="content-new-event">
		<div class="seven columns">
		<?php foreach(Event::model()->findAll(array('order'=>'id ASC, status=0 LIMIT 6'  )) as $model) { ?>
			<div class="six columns event">
					<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/event/<?php echo $model->foto; ?>">
					<p><?php echo substr($model->deskripsi,0,60).'...';?><?php echo CHtml::link('More', array('/site/detailEvent/', 'id'=>$model->id));?></p>
			</div>
		<?php } ?>
		<br>
			<button class="btn" style="margin-left:65px;margin-top:10px;">View More Event</button>
		</div>
		<div class="four columns video" style="float:right">
			<h4>Video</h4>
			<hr class="line">
				<iframe width="297" height="225" src="//www.youtube.com/embed/34ITvWIbtvc" frameborder="0" allowfullscreen></iframe>
		</div>

		<div class="four columns panel-twitter" style="float:right">
		<br>
		<br>
		
			<h4>Panel Twitter</h4>
			<hr class="line">
				<a class="twitter-timeline" href="https://twitter.com/SensasiTerabaxs" data-widget-id="544599686653288448">Tweets by @SensasiTerabaxs</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
		</div>
	</div>
	
	
</div>
<div class="hr">
</div>

