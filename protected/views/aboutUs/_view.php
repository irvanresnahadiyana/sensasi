<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('about_us')); ?>:</b>
	<?php echo CHtml::encode($data->about_us); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('visi')); ?>:</b>
	<?php echo CHtml::encode($data->visi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('misi')); ?>:</b>
	<?php echo CHtml::encode($data->misi); ?>
	<br />


</div>