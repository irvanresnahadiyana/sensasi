<?php
$this->breadcrumbs=array(
	'Videos'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Video','url'=>array('index')),
array('label'=>'Create Video','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('video-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Manage Videos</h1>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'video-grid',
'type'=>'bordered hover striped',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
		array(
			'header'=>'No',
			'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
			'htmlOptions'=>array(
				'style'=>'text-align:center',
				'width'=>'30px',
		      ),
		),
		'judul',
		'embed_code',
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
