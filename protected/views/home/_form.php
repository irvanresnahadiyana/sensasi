<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'home-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->ckEditorRow($model,'content1',array('options' => array('fullpage' => 'js:true',
										  'width' => '640',
										  'height'=>'300',
										  'resize_maxWidth' => '740',
										  'resize_minWidth' => '320'
										  ))); ?><br><br>

	<?php if($model->ilustrasi1 !=null){?>
		<img width="50%" src="<?php echo Yii::app()->baseUrl.'/images/home/'.$model->ilustrasi1;?>"><br><br>
	<?php }?>
	<?php echo $form->fileFieldRow($model,'ilustrasi1',array('class'=>'span5','maxlength'=>225)); ?><br><br>

	<?php echo $form->ckEditorRow($model,'content2',array('options' => array('fullpage' => 'js:true',
										  'width' => '640',
										  'height'=>'300',
										  'resize_maxWidth' => '740',
										  'resize_minWidth' => '320'
										  ))); ?><br><br>

	<?php if($model->ilustrasi2 !=null){?>
		<img width="50%" src="<?php echo Yii::app()->baseUrl.'/images/home/'.$model->ilustrasi2;?>"><br><br>
	<?php }?>
	<?php echo $form->fileFieldRow($model,'ilustrasi2',array('class'=>'span5','maxlength'=>225)); ?><br><br>

	<?php/* echo $form->ckEditorRow($model,'content3',array('options' => array('fullpage' => 'js:true',
										  'width' => '640',
										  'height'=>'300',
										  'resize_maxWidth' => '740',
										  'resize_minWidth' => '320'
										  ))); */?><br><br>


<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
