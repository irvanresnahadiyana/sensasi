<?php
$this->breadcrumbs=array(
	'Homes'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Home','url'=>array('index')),
array('label'=>'Create Home','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('home-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Manage Homes</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'home-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'content1',
		'ilustrasi1',
		'content2',
		'ilustrasi2',
		'content3',
		/*
		'ilustrasi3',
		*/
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
