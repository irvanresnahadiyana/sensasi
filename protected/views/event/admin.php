<?php
$this->breadcrumbs=array(
	'Events'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Event','url'=>array('index')),
array('label'=>'Create Event','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('event-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Manage Events</h1>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'event-grid',
'type'=>'hover striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'judul',
		array(
                      'name'=>'deskripsi',
                      'type'=>'raw',
                      'value'=>$model->deskripsi,
                ),
		array(
		      'name'=>'foto',
		      'type'=>'raw',
		      'value'=>'"<img width=150% src=".Yii::app()->baseUrl."/images/event/".$data->foto.">"',
		      'htmlOptions'=>array(
				'style'=>'text-align:center',
		      ),
		),
		'status',
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
