<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'event-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' =>array('enctype'=>"multipart/form-data")
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textAreaRow($model,'judul',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->ckEditorRow($model,'deskripsi',array('options' => array('fullpage' => 'js:true',
										  'width' => '640',
										  'height'=>'300',
										  'resize_maxWidth' => '740',
										  'resize_minWidth' => '320'
										  ))); ?><br><br>

	<?php echo $form->filefieldRow($model,'foto'); ?> <?php if($model->foto != ''){ echo $model->foto; };?>

	<?php echo $form->dropDownListRow($model,'status',array('0'=>'Publish', '1'=>'Un Publish')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
