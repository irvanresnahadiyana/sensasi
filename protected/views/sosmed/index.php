<?php
$this->breadcrumbs=array(
	'Sosmeds',
);

$this->menu=array(
array('label'=>'Create Sosmed','url'=>array('create')),
array('label'=>'Manage Sosmed','url'=>array('admin')),
);
?>

<h1>Sosmeds</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
