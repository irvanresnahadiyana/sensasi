<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(

	'id'=>'contac-us-form',

	'enableAjaxValidation'=>false,

)); ?>


<p class="help-block">Fields with <span class="required">*</span> are required.</p>



<?php echo $form->errorSummary($model); ?>


	<?php echo $form->ckEditorRow($model,'alamat',array('options' => array('fullpage' => 'js:true',
										  'width' => '640',
										  'height'=>'300',
										  'resize_maxWidth' => '740',
										  'resize_minWidth' => '320'
										  ))); ?><br><br>


	<?php //echo $form->textFieldRow($model,'no_hp',array('class'=>'span5','maxlength'=>255)); ?>


	<?php echo $form->ckEditorRow($model,'no_telp',array('options' => array('fullpage' => 'js:true',
										  'width' => '640',
										  'height'=>'300',
										  'resize_maxWidth' => '740',
										  'resize_minWidth' => '320'
										  ))); ?><br><br>


	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>255)); ?>


	<?php echo $form->textFieldRow($model,'website',array('class'=>'span5','maxlength'=>255)); ?>


<div class="form-actions">

	<?php $this->widget('bootstrap.widgets.TbButton', array(

			'buttonType'=>'submit',

			'type'=>'primary',

			'label'=>$model->isNewRecord ? 'Create' : 'Save',

		)); ?>
</div>



<?php $this->endWidget(); ?>
