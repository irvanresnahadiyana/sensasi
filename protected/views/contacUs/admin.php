<?php
$this->breadcrumbs=array(
	'Contac Uses'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List ContacUs','url'=>array('index')),
array('label'=>'Create ContacUs','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('contac-us-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Manage Contac Uses</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'contac-us-grid',
'type'=>'bordered hover striped',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		array(
                      'name'=>'alamat',
                      'type'=>'raw',
                      'value'=>$model->alamat,
                ),
		'no_hp',
		array(
                      'name'=>'no_telp',
                      'type'=>'raw',
                      'value'=>$model->no_telp,
                ),
		array(
                      'name'=>'email',
                      'type'=>'raw',
                      'value'=>$model->email,
                ),
		'website',
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
