<?php
$this->breadcrumbs=array(
	'Contac Uses'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List ContacUs','url'=>array('index')),
array('label'=>'Manage ContacUs','url'=>array('admin')),
);
?>

<h1>Create ContacUs</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>