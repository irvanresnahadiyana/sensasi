<?php
$this->breadcrumbs=array(
	'Galleries'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Gallery','url'=>array('index')),
array('label'=>'Create Gallery','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('gallery-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Manage Galleries</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'gallery-grid',
'type'=>'bordered hover striped',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
		array(
			'header'=>'No',
			'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
			'htmlOptions'=>array(
				'style'=>'text-align:center',
				'width'=>'30px',
		      ),
		),
		array(
		      'name'=>'gambar',
		      'type'=>'raw',
		      'value'=>'"<img width=40% src=".Yii::app()->baseUrl."/images/gallery/".$data->gambar.">"',
		      'htmlOptions'=>array(
				'style'=>'text-align:center',
		      ),
		     ),
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
