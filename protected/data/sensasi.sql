/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.5.27 : Database - sensasi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`sensasi` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `sensasi`;

/*Table structure for table `about_us` */

DROP TABLE IF EXISTS `about_us`;

CREATE TABLE `about_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `about_us` text,
  `visi` text,
  `misi` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `about_us` */

insert  into `about_us`(`id`,`about_us`,`visi`,`misi`) values (1,'<p><strong>SENSASI TERABAX&rsquo;S</strong> sebuah komunitas yang mengajarkan serta menampilkan kabaret, drama atau bidang seni peran lainnya, juga mengajarkan kreatifitas, keterampilan hidup, kesuksesan, motivasi, keorganisasian, kepemimpinan dan pengembangan diri. &nbsp;</p>\r\n\r\n<p><strong>SENSASI TERABAX&rsquo;S</strong> lahir di SMPN 3 Bandung. Setelah para pendirinya menjadi alumnus, <strong>SENSASI TERABAX&rsquo;S</strong> masih tetap tumbuh dan berkembang, karena hampir semua anggota pernah sekolah di SMPN 3 Bandung yaitu termasuk para juniornya yang &lsquo;diperoleh&rsquo; dari <em>open casting</em>.</p>\r\n\r\n<p>Taufik Herdiyana (alumnus SMPN3 2003), salah satu pengurus, mengagas pembentukan <em>SENSASI MANAGEMENT</em> sebagai salah satu upaya pelebaran sayap menuju dunia entertainment. Dengan demikian, <strong>SENSASI TERABAX&rsquo;S</strong> dapat dikatakan menjadi bagian dan &lsquo;produk&rsquo; dari <em>SENSASI MANAGEMENT</em> karena mengadopsi sebuah band <strong>Police rock n Roll.</strong></p>\r\n\r\n<p>Kini, <em>SENSASI MANAGEMENT</em> telah diubah nama menjadi <u>SENSASI PRODUCTION</u> dan menaungi <strong>SENSASI TERABAX&rsquo;S</strong>, Police Rock n Roll, KnockOut Dancer, MC, dan Promotor Agency untuk <em>sound system</em>.</p>\r\n','<p><strong>VISI</strong></p>\r\n\r\n<p>Menjadikan Sensasi Terabax&rsquo;s sebagai suatu komunitas yang memiliki sumber daya kreatif dan berkompetensi di universitas kehidupan</p>\r\n','<p><strong>MISI </strong></p>\r\n\r\n<p>1.&nbsp;&nbsp;&nbsp; Menjadikan manusia yang bertakwa kepada Tuhan Yang Maha Esa</p>\r\n\r\n<p>2.&nbsp;&nbsp;&nbsp; Menciptakan manusia yang berkompetensi di dunia entertainment, seni , dan kreatifitas</p>\r\n\r\n<p>3.&nbsp;&nbsp;&nbsp; Menjunjung tinggi profesionalitas</p>\r\n\r\n<p>4.&nbsp;&nbsp;&nbsp; Membangun relasi</p>\r\n\r\n<p>5.&nbsp;&nbsp;&nbsp; Menjadikan media edukasi yang bermanfaat bagi masyarakat</p>\r\n');

/*Table structure for table `contac_us` */

DROP TABLE IF EXISTS `contac_us`;

CREATE TABLE `contac_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alamat` text,
  `no_hp` varchar(255) DEFAULT NULL,
  `no_telp` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `contac_us` */

insert  into `contac_us`(`id`,`alamat`,`no_hp`,`no_telp`,`email`,`website`) values (1,'<p>Jl. Pertengahan No.29<br />\r\nCijantung, 13770<br />\r\nJakarta &ndash; Indonesia</p>\r\n','','<p>+6221-71203740<br />\r\n+62-821 2655 8595</p>\r\n','info@gemilangmultimedia.co.id','');

/*Table structure for table `event` */

DROP TABLE IF EXISTS `event`;

CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` text,
  `deskripsi` text,
  `foto` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `event` */

insert  into `event`(`id`,`judul`,`deskripsi`,`foto`,`status`) values (1,'Event Sensasi Kabaret','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>\r\n','1418669766_a.jpg',0),(2,'Sensasi Kabaret 2','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>\r\n','1418671423_34118_135372109814361_100000247197777_311943_3121161_n.jpg',0),(4,'Event Kabaret 4','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>\r\n','1418671651_254373_232803523397957_5425174_n.jpg',0),(5,'Event Kabaret 5','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>\r\n','1418671933_379965_317885964889712_685349957_n.jpg',0),(6,'Event Kabaret 3','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>\r\n','1418672913_DSC00840.jpg',0),(7,'Event Kabaret 6','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>\r\n','1418673013_408663_333579883320320_119996054_n.jpg',0);

/*Table structure for table `gallery` */

DROP TABLE IF EXISTS `gallery`;

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gambar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `gallery` */

insert  into `gallery`(`id`,`gambar`) values (1,'1418705891_34118_135372109814361_100000247197777_311943_3121161_n.jpg'),(3,'1418706117_260073_232800210064955_2531238_n.jpg'),(4,'1418706135_304029_287670467911262_52213812_n.jpg'),(5,'1418706149_390806_317885771556398_1452639099_n.jpg'),(6,'1418706178_1234237_714898631855108_848205995_n.jpg'),(7,'1418706195_DSC00840.jpg');

/*Table structure for table `slider` */

DROP TABLE IF EXISTS `slider`;

CREATE TABLE `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `slider` */

insert  into `slider`(`id`,`file`) values (8,'1418665585_sensasi.jpg');

/*Table structure for table `sosmed` */

DROP TABLE IF EXISTS `sosmed`;

CREATE TABLE `sosmed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `twitter` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `sosmed` */

insert  into `sosmed`(`id`,`twitter`,`facebook`) values (1,'https://twitter.com','https://facebook.com');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `last_login` timestamp NULL DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`password`,`role_id`,`status`,`last_login`,`create_time`,`update_time`,`create_user_id`,`update_user_id`) values (1,'admin','21232f297a57a5a743894a0e4a801fc3',1,1,'2014-10-02 13:09:17',NULL,NULL,NULL,NULL);

/*Table structure for table `user_role` */

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user_role` */

insert  into `user_role`(`id`,`role`) values (1,'Admin'),(2,'Operator');

/*Table structure for table `video` */

DROP TABLE IF EXISTS `video`;

CREATE TABLE `video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` text,
  `embed_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `video` */

insert  into `video`(`id`,`judul`,`embed_code`) values (1,'Cinta Mati Kabaret','<iframe src=\"//www.youtube.com/embed/hOVAVE_g2r0\" frameborder=\"0\" allowfullscreen></iframe>'),(2,'Company Profile','<iframe src=\"//www.youtube.com/embed/kp8R-F5aZnE\" frameborder=\"0\" allowfullscreen></iframe>'),(3,'Sensasi Kabaret Bandung','<iframe src=\"//www.youtube.com/embed/_8SF1DE9Dh0\" frameborder=\"0\" allowfullscreen></iframe>');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
